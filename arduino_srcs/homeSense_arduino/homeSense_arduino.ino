// Modul pro měření teploty, vlhkosti a barometrického tlaku se senzorem BME280 a posíláním naměřených hodnot na ThinkSpeak

// připojení knihovny pro WiFi
#include <ESP8266WiFi.h>

//připojení knihovny pro ThingSpeak
#include <ThingSpeak.h>

// hesla a tokeny
#include "ThingSpeakCredentials.h"
#include "WiFiCredentials.h"

// připojení potřebných knihoven pro BME280
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

// nastavení připojení na ThingSpeak
const char* apiKey = THINGSPEAK_API_KEY;
unsigned long channelNumber = THINGSPEAK_CHANNEL_NUMBER; 

// nastavení a inicializace WiFi připojení
const char* WiFiName = WIFI_NAME;
const char* WiFiPass = WIFI_PASSWORD;
WiFiClient client;

// nastavení adresy senzoru BME280
#define BME280_ADDRESS (0x76)
// inicializace senzoru BME280 z knihovny
Adafruit_BME280 bme;

void setup() {
  // inicializace komunikace po sériové lince
  Serial.begin(9600);
    
  // inicializace komunikace se senzorem BME280,
  // v případě chyby je vypsána hláška po sériové lince
  // a zastaven program
  if (!bme.begin(BME280_ADDRESS)) {
    Serial.println("BME280 senzor nenalezen, zkontrolujte zapojeni!");
    while (1);
  }

  //inicializace WiFi připojení, v případě chyby hláška
  WiFi.begin(WiFiName, WiFiPass);
  // čekání na připojení, každých 500 milisekund tečka
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Pripojeno k WiFi siti ");
  Serial.println(WiFiName);
  Serial.print("IP adresa: ");
  Serial.println(WiFi.localIP());

  ThingSpeak.begin(client);
  
}

void loop() {
  // načtení všech informací ze senzoru BME280 do proměnných
  float temperature = bme.readTemperature();
  float pressure = bme.readPressure() / 100.0F; // přepočten na hPa
  float humidity = bme.readHumidity();

  // nastavení jednotlivých polí ThingSpeaku
  ThingSpeak.setField(2, temperature);
  ThingSpeak.setField(3, pressure);
  ThingSpeak.setField(4, humidity);

  // Zapsat data na ThingSpeak
  int x = ThingSpeak.writeFields(channelNumber, apiKey);
  if(x == 200){
    Serial.println("Data odeslana na ThingSpeak.");
  }
  else{
    Serial.println("Problem pri nahravani dat. HTTP error code " + String(x));
  }
  
  // výpis do Serial monitoru
  Serial.println("Teplota: "+String(temperature)+"°C");
  Serial.println("Tlak: "+String(pressure)+"hPa");
  Serial.println("Vlhkost: "+String(humidity)+"%");

  // vytištění prázdného řádku a pauza po dobu 12 vteřin
  Serial.println();
  Serial.println("Cekam 12 vterin pred odeslanim dalsich dat.");
  delay(12000);
}
